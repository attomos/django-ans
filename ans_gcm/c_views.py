from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from models import ANSUser, Circle, Membership
from forms import CircleForm, MembershipForm


def home(request, template='home.html'):
    # assuming everyone is book
    # gonna change to request.user.username
    user = ANSUser.objects.get(id=4)
    list_of_circles = Circle.objects.filter(owner=user)
    return render_to_response(template, {
            'current_user': user,
            'list_of_circles': list_of_circles,
        }, RequestContext(request))


def circle(request, id, template='circle.html'):
    form = MembershipForm()
    try:
        circle = Circle.objects.get(id=id)
    except Circle.DoesNotExist:
        raise Http404
    member_ships = []
    #for member in circle.members.all():
    for member in Membership.objects.filter(circle=circle):
        member_ships.append(member)
    if request.POST:
        form = MembershipForm(request.POST)
        if form.is_valid():
            new_member = form.save(commit=False)
            new_member.circle = circle
            for m in member_ships:
                # existing is not allow
                if new_member.ans_user == m.ans_user:
                    return HttpResponseRedirect(reverse('circle', args=id))
            new_member.save()
            return HttpResponseRedirect(reverse('circle', args=id))
    return render_to_response(template, {
            'circle': circle,
            'form': form,
            'member_ships': member_ships,
        }, RequestContext(request))


def add_circle(request, template='add_circle.html'):
    # assuming everyone is book
    # gonna change to request.user.username
    user = ANSUser.objects.get(id=4)
    if request.POST:
        form = CircleForm(request.POST)
        if form.is_valid():
            new_circle = form.save(commit=False)
            # owner is the request user
            new_circle.owner = user
            new_circle.save()
            return HttpResponseRedirect('/')
    else:
        form = CircleForm()
    return render_to_response(template, {
            'form': form,
        }, RequestContext(request))


def edit_circle(request, id, template='edit_circle.html'):
    circle = Circle.objects.get(id=id)
    if request.POST:
        form = CircleForm(request.POST, instance=circle)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('circle', args=id))
    else:
        form = CircleForm(instance=circle)
    return render_to_response(template, {
            'form': form,
        }, RequestContext(request))


def delete_circle(request, id):
    circle = Circle.objects.get(id=id)
    circle.delete()
    return HttpResponseRedirect('/')


def add_or_remove_from_circle(request, id):
    return None


def delete_member(request):
    return HttpResponse()
