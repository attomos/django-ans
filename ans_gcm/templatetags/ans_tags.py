from classytags.core import Tag, Options
from classytags.arguments import Argument
from django import template

register = template.Library()


@register.simple_tag
def get_fullname(user):
    return user.get_fullname()


@register.simple_tag
def get_type(user):
    return user.get_type()


@register.simple_tag
def get_status(user):
    return user.get_status()


@register.simple_tag
def no_of_member(group):
    total = len(group.members.all())
    if total > 1:
        return '%d %s' % (total, ' members')
    return '%d %s' % (total, ' member')


@register.simple_tag
def member_of(group):
    """
    No need to use
    """
    member_list = []
    for member in group.members.all():
        member_list.append(member.first_name + member.last_name)
    return member_list


class Hello(Tag):
    options = Options(
            Argument('name', required=False, default='world'),
            'as',
            Argument('varname', required=False, resolve=False)
    )

    def render_tag(self, context, name, varname):
        output = 'hello %s' % name
        if varname:
            context[varname] = output
            return ''
        return output
register.tag(Hello)
