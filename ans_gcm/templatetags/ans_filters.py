from django import template

register = template.Library()


@register.filter
def is_sender(value, arg):
    return value == arg
