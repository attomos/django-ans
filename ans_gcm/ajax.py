from django.core.urlresolvers import reverse
from dajax.core import Dajax
from dajaxice.decorators import dajaxice_register
from models import ANSUser, Group


@dajaxice_register
def multiply(request, a, b):
    dajax = Dajax()
    try:
        result = int(a) * int(b)
    except:
        result = ''
    dajax.assign('#result', 'value', str(result))
    dajax.clear('#loader', 'innerHTML')
    dajax.assign('#button', 'disabled', '')
    return dajax.json()


@dajaxice_register
def create_group(request, group_name):
    dajax = Dajax()
    ans_user = ANSUser.objects.get(username=request.user.username)

    group_name_list = []
    groups_by_user = Group.objects.filter(owner=ans_user)
    for group in groups_by_user:
        group_name_list.append(group.name)
    if group_name in group_name_list:
        dajax.clear('#loader', 'innerHTML')
        dajax.add_css_class('#loader', 'text-error')
        dajax.assign('#loader', 'innerHTML',
                'group %s already exist' % group_name)
        return dajax.json()

    new_group = Group(owner=ans_user, name=group_name)
    new_group.save()
    dajax.clear('#loader', 'innerHTML')
    dajax.redirect(reverse('groups'))
    return dajax.json()


@dajaxice_register
def edit_group(request, group_id, group_name):
    dajax = Dajax()

    group = Group.objects.get(id=group_id)
    group.name = group_name
    group.save()

    dajax.clear('#loader', 'innerHTML')
    dajax.redirect(reverse('groups'))
    return dajax.json()


@dajaxice_register
def delete_group(request, group_id):
    dajax = Dajax()

    group = Group.objects.get(id=group_id)
    group.delete()

    dajax.clear('#loader', 'innerHTML')
    dajax.redirect(reverse('groups'))
    return dajax.json()
