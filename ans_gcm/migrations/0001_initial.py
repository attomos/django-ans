# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Agenda'
        db.create_table('ans_gcm_agenda', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('message_id', self.gf('django.db.models.fields.related.OneToOneField')(related_name='agenda', unique=True, to=orm['ans_gcm.Message'])),
            ('datetime', self.gf('django.db.models.fields.DateTimeField')()),
            ('confirm', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('place', self.gf('django.db.models.fields.CharField')(max_length=140)),
        ))
        db.send_create_signal('ans_gcm', ['Agenda'])

        # Adding model 'ANSUser'
        db.create_table('ans_gcm_ansuser', (
            ('user_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True, primary_key=True)),
            ('user_type', self.gf('django.db.models.fields.IntegerField')()),
            ('profile_picture', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('asterisk_username', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('status', self.gf('django.db.models.fields.IntegerField')(default=2)),
            ('location', self.gf('django.db.models.fields.CharField')(max_length=140, null=True, blank=True)),
        ))
        db.send_create_signal('ans_gcm', ['ANSUser'])

        # Adding model 'Message'
        db.create_table('ans_gcm_message', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('device', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ans_gcm.Device'], null=True, blank=True)),
            ('body', self.gf('django.db.models.fields.TextField')(max_length=140)),
            ('sender', self.gf('django.db.models.fields.related.ForeignKey')(related_name='received_messages', to=orm['ans_gcm.ANSUser'])),
            ('recipient', self.gf('django.db.models.fields.related.ForeignKey')(related_name='sent_message', to=orm['ans_gcm.ANSUser'])),
            ('created_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('sent_from', self.gf('django.db.models.fields.CharField')(default=None, max_length=20, null=True, blank=True)),
            ('missed_call_photo', self.gf('django.db.models.fields.files.ImageField')(default=None, max_length=100, null=True, blank=True)),
            ('message_status', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('ans_gcm', ['Message'])

        # Adding model 'Group'
        db.create_table('ans_gcm_group', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=140)),
            ('owner', self.gf('django.db.models.fields.related.ForeignKey')(related_name='has', to=orm['ans_gcm.ANSUser'])),
        ))
        db.send_create_signal('ans_gcm', ['Group'])

        # Adding model 'Membership'
        db.create_table('ans_gcm_membership', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ans_user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ans_gcm.ANSUser'])),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ans_gcm.Group'])),
            ('date_joined', self.gf('django.db.models.fields.DateField')(auto_now=True, auto_now_add=True, blank=True)),
        ))
        db.send_create_signal('ans_gcm', ['Membership'])

        # Adding model 'Device'
        db.create_table('ans_gcm_device', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('identifier', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('registration_id', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ans_gcm.ANSUser'], null=True, blank=True)),
            ('registered_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('modified_on', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('account_name', self.gf('django.db.models.fields.CharField')(max_length=120)),
        ))
        db.send_create_signal('ans_gcm', ['Device'])

        # Adding model 'ApiKey'
        db.create_table('ans_gcm_apikey', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='keys', unique=True, to=orm['auth.User'])),
            ('key', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
        ))
        db.send_create_signal('ans_gcm', ['ApiKey'])


    def backwards(self, orm):
        # Deleting model 'Agenda'
        db.delete_table('ans_gcm_agenda')

        # Deleting model 'ANSUser'
        db.delete_table('ans_gcm_ansuser')

        # Deleting model 'Message'
        db.delete_table('ans_gcm_message')

        # Deleting model 'Group'
        db.delete_table('ans_gcm_group')

        # Deleting model 'Membership'
        db.delete_table('ans_gcm_membership')

        # Deleting model 'Device'
        db.delete_table('ans_gcm_device')

        # Deleting model 'ApiKey'
        db.delete_table('ans_gcm_apikey')


    models = {
        'ans_gcm.agenda': {
            'Meta': {'object_name': 'Agenda'},
            'confirm': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'datetime': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message_id': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'agenda'", 'unique': 'True', 'to': "orm['ans_gcm.Message']"}),
            'place': ('django.db.models.fields.CharField', [], {'max_length': '140'})
        },
        'ans_gcm.ansuser': {
            'Meta': {'object_name': 'ANSUser', '_ormbases': ['auth.User']},
            'asterisk_username': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '140', 'null': 'True', 'blank': 'True'}),
            'profile_picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '2'}),
            'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'primary_key': 'True'}),
            'user_type': ('django.db.models.fields.IntegerField', [], {})
        },
        'ans_gcm.apikey': {
            'Meta': {'object_name': 'ApiKey'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'keys'", 'unique': 'True', 'to': "orm['auth.User']"})
        },
        'ans_gcm.device': {
            'Meta': {'ordering': "['-modified_on']", 'object_name': 'Device'},
            'account_name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identifier': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'registered_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'registration_id': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ans_gcm.ANSUser']", 'null': 'True', 'blank': 'True'})
        },
        'ans_gcm.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'members': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['ans_gcm.ANSUser']", 'through': "orm['ans_gcm.Membership']", 'symmetrical': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '140'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'has'", 'to': "orm['ans_gcm.ANSUser']"})
        },
        'ans_gcm.membership': {
            'Meta': {'object_name': 'Membership'},
            'ans_user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ans_gcm.ANSUser']"}),
            'date_joined': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ans_gcm.Group']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'ans_gcm.message': {
            'Meta': {'object_name': 'Message'},
            'body': ('django.db.models.fields.TextField', [], {'max_length': '140'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'device': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ans_gcm.Device']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message_status': ('django.db.models.fields.IntegerField', [], {}),
            'missed_call_photo': ('django.db.models.fields.files.ImageField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'recipient': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'sent_message'", 'to': "orm['ans_gcm.ANSUser']"}),
            'sender': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'received_messages'", 'to': "orm['ans_gcm.ANSUser']"}),
            'sent_from': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '20', 'null': 'True', 'blank': 'True'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['ans_gcm']