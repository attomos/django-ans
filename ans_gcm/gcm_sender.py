# -*- encoding: utf-8 -*-
import urllib
import urllib2
import simplejson as json


def send_gcm_message(api_key, registration_id, data, collapse_key=None):
    values = {
        "registration_id": registration_id,
        "collapse_key": collapse_key,
        "data": data,
    }

    data = urllib.urlencode(values)

    headers = {
        'UserAgent': "GCM-Server",
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        'Authorization': 'key=' + api_key,
        'Content-Length': str(len(data)),
    }

    request = urllib2.Request("https://android.googleapis.com/gcm/send",
            data, headers)
    response = urllib2.urlopen(request)
    result = response.read()

    return result


def send_json_gcm_message(api_key, json_data):
    headers = {
        'UserAgent': "GCM-Server",
        'Content-Type': "application/json",
        'Authorization': 'key=' + api_key,
    }

    request = urllib2.Request("https://android.googleapis.com/gcm/send",
            json_data, headers)
    response = urllib2.urlopen(request)
    result = response.read()

    return result


'''
json_data = json.dumps({"collapse_key": "message",
    "time_to_live": 108,
    "data": {
        "message": "test",
        "agenda": "tomorrow, noon"
    }, "registration_ids": [
            "APA91bH1smP6voIosXSt4KHfWS5OBi0zid36r4X4pbdzQcA5oBf_1Kwvs3-AgW1qIXfR4EAKkHRNsx2ejcQ5LsKL8eNP8p-DYc1WFjMukvLdzISHX5PLpoCx5LNKbvQRb7n9dJ2ENWmi",
        ]
    })
'''
'''
print send_json_gcm_message('AIzaSyBzSo02XwIaWzkPcnIf-Vy_ni7msueV_bU', json_data)
'''
