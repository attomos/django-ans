from django.db import models
from django.contrib.auth.models import User


class Circle(models.Model):
    name = models.CharField(max_length=140)
    members = models.ManyToManyField('ANSUser', through='Membership')
    owner = models.ForeignKey('ANSUser', related_name='has')

    def __unicode__(self):
        return self.name


class Membership(models.Model):
    ans_user = models.ForeignKey('ANSUser')
    circle = models.ForeignKey('Circle')
    date_joined = models.DateField(auto_now_add=True, auto_now=True)

    @classmethod
    def is_owner(self):
        return str(self.ans_user) == str(self.circle.owner)

    '''
    def save(self, *args, **kwargs):
        if str(self.ans_user) != str(self.circle.owner):
            print(str(self.ans_user) != str(self.circle.owner))
            print(self.ans_user)
            print(self.circle.owner)
            super(Membership, self).save(*args, **kwargs)
            # Call the "real" save() method.
        else:
            print(args)
            print(kwargs)
    '''


class ANSUser(User):
    """
    Class description for ANSUser.
    """

    @classmethod
    def upload_to_username(self):
        return 'profile_pictues/%s' % self.username

    def __unicode__(self):
        return self.username
