# -*- encoding: utf-8 -*-
from django.conf import settings
from django.contrib.admin.widgets import AdminFileWidget
from django.utils.safestring import mark_safe

from easy_thumbnails.files import get_thumbnailer


class AdminImageWidget(AdminFileWidget):
    def render(self, name, value, attrs=None):
        output = []
        if value and getattr(value, "url", None):
            #image_url = value.url
            file_name = str(value)
            user_name = file_name.split('/')[1]
            file_extension = file_name.split('/')[2]
            #output.append(image_url + '<br />' + file_name)
            try:
                picture = open(settings.MEDIA_ROOT + '/' + file_name)
                thumbnailer = get_thumbnailer(picture,
                        relative_name=('avatars/%s/%s' %
                            (user_name, file_extension)))
                        #'avatars/' + user_name + '/' +
                         #   file_extension))
                         # TODO(me) just in case
                thumb = thumbnailer.get_thumbnail({'size': (149, 149),
                    'crop': True})
                output.append(
                        u'<a href="%s" target="_blank">'
                                '<img src="%s" alt="%s" /></a> %s ' % (
                                    thumb.url, thumb.url, file_name, '')
                                )
            except IOError:
                pass
        else:
            # handle coercing to Unicode: need string or buffer, NoneType found
            stub_image_url = settings.STATIC_URL + 'images/profile_pic1.jpg'
            output.append(
                    u' <a href="%s" target="_blank">'
                    '<img src="%s" alt="%s" /></a> %s ' % (
                        stub_image_url, stub_image_url, 'stub', ''))
        output.append(super(AdminFileWidget, self).render(
            name, value, attrs))
        return mark_safe(u''.join(output))
