# -*- encoding: utf-8 -*-
from django.db import models
from django.core.files.storage import default_storage
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from datetime import datetime
#from pytz import timezone

from gcm_sender import send_gcm_message
from easy_thumbnails.fields import ThumbnailerImageField

KEY_SIZE = 50

LOCATIONS = (
    (u'CB1', 'CB1'),
    (u'CB2', 'CB2'),
    (u'KMUTT Library', 'KMUTT Library'),
    (u'LNG', 'LNG'),
    (u'SIT Building', 'SIT Building'),
    (u'SIT Library', 'SIT Library'),
)


def avatar_path(instance, filename):
    avatar_path = '/'.join(['avatars', instance.username, filename])
    if default_storage.exists(avatar_path):
        default_storage.delete(avatar_path)
    return avatar_path


def missed_call_photo_path(instance, filename):
    missed_call_photo_path = '/'.join(
            ['missed_call_photo', instance.recipient.username, filename])
    if default_storage.exists(missed_call_photo_path):
        default_storage.delete(missed_call_photo_path)
    return missed_call_photo_path


class Agenda(models.Model):
    """
    Agenda model for ANS.
    """

    #agenda_id = models.AutoField('agenda id', primary_key=True) NO NEED REALLY
    message_id = models.OneToOneField('Message', related_name='agenda')
    datetime = models.DateTimeField('Appointment date')
    confirm = models.BooleanField(default=False)
    place = models.CharField(max_length=140, choices=LOCATIONS)

    def __unicode__(self):
        return self.place + '_' + str(self.id)

    def get_message_header(self):
        return self.message_id
    get_message_header.short_description = "Message ID"

    def is_today(self):
        return self.datetime.date() == datetime.today().date()
        #return self.datetime.astimezone(timezone('Asia/Bangkok')).date()
        # == datetime.today().date()
    is_today.short_description = 'Today?'

    def get_confirm(self):
        if self.confirm:
            return "confirmed"
        else:
            return "not confirmed"


class ANSUser(User):
    """
    Class description for ANSUser.
    """
    USER_STATUSES = (
        (0, 'Available'),
        (1, 'Busy'),
        (2, 'Appear offline'),
    )

    USER_TYPES = (
        (0, 'Lecturer'),
        (1, 'Staff'),
        (2, 'Student'),
    )

    user_type = models.IntegerField(choices=USER_TYPES,
            help_text='User type')

    @classmethod
    def upload_to_username(self):
        return 'profile_pictues/%s' % self.username

    profile_picture = ThumbnailerImageField(upload_to=avatar_path,
            null=True, blank=True,
            help_text='Profile picture use for identify the user.')
    asterisk_username = models.CharField(max_length=30, null=True, blank=True,
            help_text='Asterisk extension, please consult your VoIP admin.' +
            ' (lecturer or staff only)')
    status = models.IntegerField(
            default=2,
            choices=USER_STATUSES,
            help_text="Status can be Available, Busy, or Appear offline.")
    location = models.CharField(max_length=140, null=True,
            blank=True, help_text="Latest location.")
    '''
    phone_number = models.CharField(max_length=20)
    '''

    def __unicode__(self):
        return self.username

    def get_fullname(self):
        fullname = self.first_name + ' ' + self.last_name
        return fullname

    def get_type(self):
        return self.USER_TYPES[self.user_type][1]

    def get_status(self):
        return self.USER_STATUSES[self.status][1]


class Message(models.Model):
    """
    Class description for Message.
    """

    NORMAL = 0
    MISSED_CALL = 1
    MESSAGE_STATUSES = (
        (NORMAL, 'Normal'),
        (MISSED_CALL, 'Missed call'),
    )
    device = models.ForeignKey('Device', null=True, blank=True)
    body = models.TextField(max_length=140)
    sender = models.ForeignKey('ANSUser', related_name='received_messages',
            null=False)
    recipient = models.ForeignKey('ANSUser', related_name='sent_message',
            null=False)
    created_on = models.DateTimeField(auto_now_add=True, editable=False)
    sent_from = models.CharField(max_length=20, null=True, blank=True,
            default=None)
    missed_call_photo = models.ImageField(upload_to=missed_call_photo_path,
            null=True, blank=True, default=None)
    message_status = models.IntegerField(
            choices=MESSAGE_STATUSES,
            help_text="Normal or Missed call")

    def __unicode__(self):
        return self.body

    def get_title(self):
        if len(self.body) < 15:
            return self.body
        elif len(self.body) >= 15:
            return self.body[0: 12] + '...'

    def get_agenda(self):
        return Agenda.objects.get(message_id=self)
    get_agenda.short_description = "Agenda"


class Group(models.Model):
    name = models.CharField(max_length=140)
    members = models.ManyToManyField('ANSUser', through='Membership')
    owner = models.ForeignKey('ANSUser', related_name='has')

    def __unicode__(self):
        return self.name


class Membership(models.Model):
    ans_user = models.ForeignKey('ANSUser')
    group = models.ForeignKey('Group')
    date_joined = models.DateField(auto_now_add=True, auto_now=True)

    @classmethod
    def is_owner(self):
        return str(self.ans_user) == str(self.group.owner)


class Device(models.Model):
    identifier = models.CharField(max_length=200,
            verbose_name='Device ID')  # UID of phone/device
    registration_id = models.CharField(max_length=200,
            verbose_name='Registration ID')
    # GCM registration ID forwarded by phone
    user = models.ForeignKey('ANSUser', null=True, blank=True)
    registered_on = models.DateTimeField(verbose_name="Registered on",
            auto_now_add=True)
    modified_on = models.DateTimeField(verbose_name="Modified on",
            auto_now=True)
    account_name = models.CharField(max_length=120)

    class Meta:
        verbose_name = _("Device")
        verbose_name_plural = _("Devices")
        ordering = ['-modified_on']

    def __unicode__(self):
        return self.identifier

    def shorten_registration_id(self):
        return self.registration_id[:30] + '...'

    @property
    def is_registered(self):
        """
        Check if we can send messsage to this device.
        """
        pass

    def send_message(self, message):
        """
        Send message to the current device.
        """
        return send_gcm_message(api_key=settings.GCM_APIKEY,
                registration_id=self.registration_id,
                data='{"message": "%s"}' % message, collapse_key="message")


class ApiKey(models.Model):
    user = models.ForeignKey(User, related_name='keys', unique=True)
    key = models.CharField(max_length=KEY_SIZE, null=True, blank=True)

    def save(self, *args, **kwargs):
        self.key = User.objects.make_random_password(length=KEY_SIZE)

        while ApiKey.objects.filter(key__exact=self.key).count():
            self.key = User.objects.make_random_password(length=KEY_SIZE)

        super(ApiKey, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.key
