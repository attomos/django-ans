# -*- encoding: utf-8 -*-
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse

#from django.core.files.base import ContentFile
#from django.conf import settings
from django.db.models import Q
from django.shortcuts import render, render_to_response
from django.template.context import RequestContext
#from django.views.generic.simple import redirect_to
from django.views.decorators.csrf import csrf_exempt

from models import ANSUser, Message, Group, Agenda
from api.handlers import UserHandler, MessageHandler2
from api.handlers import MessageHandler3, AgendaHandler
from api.handlers import ConversationHandler, GroupsHandler


def new_index(request, template='new_index.html'):
    return render_to_response(template, {}, RequestContext(request))


def agenda(request, template='agenda.html'):
    if request.user.is_superuser:
        return HttpResponseRedirect("/admin")
    agenda_handler = AgendaHandler()
    agendas_list = agenda_handler.read(request)
    request.session['agendas_list'] = agendas_list
    return render_to_response(template, {
            }, RequestContext(request))


def approve_agenda(request, id):
    agenda = Agenda.objects.get(id=id)
    agenda.confirm = True
    agenda.save()
    return HttpResponseRedirect(reverse('messages'))


def delete_agenda(request, id):
    agenda = Agenda.objects.get(id=id)
    agenda.confirm = True
    agenda.save()
    return HttpResponseRedirect(reverse('messages'))


def conversation(request, id, template='conversation.html'):
    """
    Conversation between request user and specified user
    """
    if request.user.is_superuser:
        return HttpResponseRedirect("/admin")
    sender = ANSUser.objects.get(pk=id)
    '''
    recipient = ANSUser.objects.get(username=request.user.username)
    latest_messages = Message.objects.filter(
            Q(recipient=recipient, sender=sender) |
            Q(recipient=sender, sender=recipient)).order_by('-created_on')
    '''
    conversation_handler = ConversationHandler()
    latest_messages = conversation_handler.read(request, id)

    request.session['conversation_messages'] = latest_messages
    return render_to_response(template, {
            'sender': sender,
            #'conversation_messages': latest_messages
            }, RequestContext(request))


def contacts(request, template='contacts.html'):
    if request.user.is_superuser:
        return HttpResponseRedirect("/admin")
    contacts_list = ANSUser.objects.filter(~Q(username='asterisk'),
            ~Q(username=request.user)).order_by('first_name', 'last_name')
    request.session['contacts_list'] = contacts_list
    return render_to_response(template, {
            }, RequestContext(request))


def favorite_contacts(request):
    if request.user.is_superuser:
        return HttpResponseRedirect("/admin")
    return render_to_response('fav_contact.html', {
            }, RequestContext(request))


@csrf_exempt
def messages(request, template='all_messages.html'):
    if request.user.is_superuser:
        return HttpResponseRedirect("/admin")
    latest_messages_handler = MessageHandler3()
    latest_messages = latest_messages_handler.read(request)
    request.session['latest_messages'] = latest_messages
    return render_to_response(template, {
            'latest_messages': request.session['latest_messages'],
            }, context_instance=RequestContext(request))


def missed_call_messages(request, template='misscall_message.html'):
    if request.user.is_superuser:
        return HttpResponseRedirect("/admin")
    request_user = ANSUser.objects.get(username=request.user.username)
    missed_call_messages = Message.objects.filter(recipient=request_user,
            message_status=1).order_by('-created_on')
    request.session['missed_call_messages'] = missed_call_messages
    return render_to_response(template, {
        }, RequestContext(request))


def send_message(request, template='sendmessage.html'):
    if request.user.is_superuser:
        return HttpResponseRedirect("/admin")
    user_handler = UserHandler()
    users_list = user_handler.read(request)
    if request.GET and 'recipient' in request.GET:
        recipient = request.GET.get('recipient')
        return render_to_response('sendmessage.html', {
            'users_list': list(str(user.username) for user in users_list),
            'recipient': recipient
        }, RequestContext(request))
    if request.POST:
        recipient = request.POST.get('recipient')
        message_handler = MessageHandler2()
        # send message
        message_handler.create(request, recipient)
        return HttpResponseRedirect(reverse('messages'))
    return render_to_response(template, {
        }, RequestContext(request))


def reply_message(request):
    '''
    Reply message from missed call messages view and all messages view
    '''
    if request.user.is_superuser:
        return HttpResponseRedirect("/admin")
    if request.POST:
        recipient = request.POST.get('recipient')
        message_handler = MessageHandler2()
        return HttpResponse(message_handler.create(request, recipient))


def update_profile(request):
    if request.user.is_superuser:
        return HttpResponseRedirect("/admin")
    user = ANSUser.objects.get(username=request.user.username)
    if 'availability' in request.POST:
        status = request.POST['availability']
        user.status = status
    if 'other_place' in request.POST:
        place = request.POST['other_place']
        user.location = place
    elif 'place' in request.POST:
        place = request.POST['place']
        user.location = place
    user.save()
    return HttpResponseRedirect("/")


def group(request, id, template='group.html'):
    groups_handler = GroupsHandler()
    member_ships = groups_handler.read(request, id)
    group = Group.objects.get(pk=id)
    request.session['member_ships'] = member_ships
    request.session['group_id'] = int(id)
    request.session['group'] = group
    return render_to_response(template, {},
            RequestContext(request))


def groups(request, template='groups.html'):
    if request.user.is_superuser:
        return HttpResponseRedirect("/admin")
    user = ANSUser.objects.get(username=request.user.username)
    groups = Group.objects.filter(owner=user)
    request.session['groups'] = groups
    return render_to_response(template, {},
            RequestContext(request))


def add_group(request):
    # assuming everyone is book
    # gonna change to request.user.username
    user = ANSUser.objects.get(username=request.user.username)
    if request.POST:
        print user
        return HttpResponse()


def downloads(request, template='downloads.html'):
    if request.user.is_superuser:
        return HttpResponseRedirect("/admin")
    return render(request, template)


def help(request, template='help.html'):
    return render(request, template)
