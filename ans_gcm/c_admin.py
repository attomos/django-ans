from django.contrib import admin, messages
from django.db.models import Q
from circles.models import ANSUser, Circle, Membership


class ANSUserAdmin(admin.ModelAdmin):
    list_display = ('id', 'username',
            'first_name', 'last_name', 'email',)


class ANSUserInline(admin.TabularInline):
    model = ANSUser
    fields = ('username', 'first_name', 'last_name', 'email')


class CircleAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'owner')


class MembershipAdmin(admin.ModelAdmin):
    list_display = ('id', 'ans_user', 'circle', 'date_joined')

    def save_model(self, request, obj, form, change):
        if obj.ans_user != obj.circle.owner:
            super(MembershipAdmin, self).save_model(request, obj, form, change)
        else:
            messages.error(request, 'Owner cannot be the member')

admin.site.register(ANSUser, ANSUserAdmin)
admin.site.register(Circle, CircleAdmin)
admin.site.register(Membership, MembershipAdmin)
