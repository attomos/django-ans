# -*- encoding: utf-8 -*-
from ans_gcm.models import Agenda, ANSUser, ApiKey, Device
from ans_gcm.models import Message, Group, Membership
from django.contrib import admin, messages
from django.contrib.auth.admin import UserAdmin
#from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from widgets import AdminImageWidget


class AgendaInline(admin.TabularInline):
    model = Agenda
    extra = 1
    editable = False


class AgendaAdmin(admin.ModelAdmin):
    date_hierarchy = 'datetime'
    list_display = ('id', '__unicode__', 'get_message_header',
            'datetime', 'is_today', 'confirm', 'place')
    list_display_links = ['id', '__unicode__']
    list_filter = ('datetime', 'confirm', 'place')
    search_fields = ('datetime', 'place')

    # cannot edit data in admin page
    '''
    def __init__(self, *args, **kwargs):
        super(AgendaAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = (None, )
    '''


class ANSUserAdmin(admin.ModelAdmin):
    fields = ['profile_picture', 'username', 'asterisk_username',
            'first_name', 'last_name', 'email', 'status', 'location',
            'user_type']
    #readonly_fields = ('username',)
    list_display = ('id', 'username', 'asterisk_username', 'first_name',
            'last_name', 'email', 'status', 'location', 'user_type')
    list_display_links = ['id', 'username']
    list_filter = ['username']
    search_fields = ('username', 'first_name', 'last_name', 'email')

    # good for profile picture
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'profile_picture':
            request = kwargs.pop('request', None)
            print request
            kwargs['widget'] = AdminImageWidget
            return db_field.formfield(**kwargs)
        return super(ANSUserAdmin, self).formfield_for_dbfield(
                db_field, **kwargs)


class ApiKeyAdmin(admin.ModelAdmin):
    fields = ['user', '__unicode__']
    list_display = ['user', '__unicode__']
    readonly_fields = ('user', '__unicode__', )
    list_display_links = ['user', '__unicode__']


class GroupAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'owner')


class MembershipAdmin(admin.ModelAdmin):
    list_display = ('id', 'ans_user', 'group', 'date_joined')

    def save_model(self, request, obj, form, change):
        if obj.ans_user != obj.group.owner:
            super(MembershipAdmin, self).save_model(request, obj, form, change)
        else:
            messages.error(request, 'Owner cannot be the member')


class DeviceAdmin(admin.ModelAdmin):
    list_display = ['identifier', 'user', 'shorten_registration_id',
            'account_name', 'registered_on', 'modified_on']
    search_fields = ('identifier', 'account_name', 'user')
    readonly_fields = ('identifier', 'registration_id', 'user', 'account_name')
    actions = ['send_test_message']

    def send_test_message(self, request, queryset):
        for device in queryset:
            device.send_message('GCM Test message from ANS admin')
        self.message_user(request, _("All message were sent."))
    send_test_message.short_description = _("Send test message")


class MessageAdmin(admin.ModelAdmin):
    fields = ['sender', 'recipient', 'body', 'message_status',
            'missed_call_photo', 'device']
    #inlines = [SenderInline, RecipientInline, AgendaInline]
    inlines = [AgendaInline]
    list_display = ['id', 'body', 'sender', 'recipient', 'sent_from',
            'get_agenda', 'message_status', 'created_on']
    #list_display_links = ['__unicode__']

admin.site.register(Agenda, AgendaAdmin)
admin.site.register(ANSUser, ANSUserAdmin)
admin.site.register(Message, MessageAdmin)
admin.site.register(Device, DeviceAdmin)
admin.site.register(ApiKey, ApiKeyAdmin)
admin.site.register(Group, GroupAdmin)
admin.site.register(Membership, MembershipAdmin)

# custom the auth user admin page
UserAdmin.readonly_fields = ('username', )
