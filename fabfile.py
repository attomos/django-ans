from fabric.api import *
from fabric.colors import green  # yellow, red
from fabric.contrib import django

env.hosts = ['attomos@attomos.webfactional.com']
django.project('myproject')

from ans_gcm.models import ANSUser


def commit():
    print(green('git add . && git commit'))
    local('git add . && git commit')


def push():
    print(green('git push'))
    local('git push')


def prepare_deploy():
    commit()
    push()


def syncdb():
    code_dir = '/home/attomos/webapps/ans/myproject'
    with cd(code_dir):
        run('./manage.py syncdb')


def syncdb_local():
    local('./manage.py syncdb')


def do_deploy():
    prepare_deploy()
    deploy()


def deploy():
    code_dir = '/home/attomos/webapps/ans/myproject'
    with cd(code_dir):
        run("git pull")
        run("touch myproject/wsgi.py")


def memory_usage():
    with cd('~'):
        run("fab memory_usage")
