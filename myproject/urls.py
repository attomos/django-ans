from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from dajaxice.core import dajaxice_autodiscover, dajaxice_config
dajaxice_autodiscover()
from django.conf.urls import patterns, include, url
#from django.views.generic.simple import redirect_to
from django.views.generic import RedirectView
from views import home

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns(
    '',
    # Examples:
    url(r'^$', home, name='home'),
    url(dajaxice_config.dajaxice_url, include('dajaxice.urls')),
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^new_index/$', 'ans_gcm.views.new_index', name='new_index'),

    url(r'^agenda/$', 'ans_gcm.views.agenda',
        name='agenda'),

    url(r'^approve_agenda/(?P<id>\d+)/$', 'ans_gcm.views.approve_agenda',
        name='approve_agenda'),

    url(r'^conversation/(?P<id>\d+)/$', 'ans_gcm.views.conversation',
        name='conversation'),

    url(r'^contacts/$', 'ans_gcm.views.contacts',
        name='contacts'),

    url(r'^contacts/favorite/$', 'ans_gcm.views.favorite_contacts',
        name='favorite_contacts'),

    url(r'^groups/$', 'ans_gcm.views.groups',
        name='groups'),

    url(r'^group/(?P<id>\d+)/$', 'ans_gcm.views.group',
        name='group'),

    url(r'^messages/$', 'ans_gcm.views.messages',
        name='messages'),

    url(r'^messages/missed_call/$', 'ans_gcm.views.missed_call_messages',
        name='missed_call.messages'),

    url(r'^messages/send/$', 'ans_gcm.views.send_message',
        name='messages.send'),

    url(r'^messages/reply/$', 'ans_gcm.views.reply_message',
        name='messages.reply'),

    url(r'^update_profile/$', 'ans_gcm.views.update_profile',
        name='update_profile'),

    # utilities

    url(r'^help/$', 'ans_gcm.views.help',
        name='help'),

    url(r'^downloads/$', 'ans_gcm.views.downloads',
        name='downloads'),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^api/', include('api.urls')),

    url(r'^accounts/', include('accounts.urls')),

    url(r'^login', RedirectView.as_view(url='/accounts/login?next=/'),
        name='ans.login'),

    url(r'^logout', RedirectView.as_view(url='/accounts/logout/?next=/'),
        name='ans.logout'),

    url(r'^test/$', 'myproject.views.test',
        name='test'),

    # Serve media directory
    url(r'^media(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT, }),
)

urlpatterns += staticfiles_urlpatterns()
