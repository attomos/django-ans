# -*- coding: utf-8 -*-
from django.contrib.auth.forms import UserCreationForm
from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Submit, HTML, Button, Row, Field
from crispy_forms.bootstrap import AppendedText, PrependedText, FormActions


class MessageForm(forms.Form):

    """
    full_name = forms.CharField(label='')
    # full_name if no spaces add to first_name


    email = forms.CharField(label='')
    email.widget.attrs['class'] = 'input-small'
    email.widget.attrs['placeholder'] = 'Email'

    email_domain = forms.ChoiceField(
        label='',
        choices=(('st.sit.kmutt.ac.th', 'st.sit.kmutt.ac.th'),
                 ('sit.kmutt.ac.th', 'sit.kmutt.ac.th'),
                 ('vip.sit.kmutt.ac.th', 'vip.sit.kmutt.ac.th'),
                 ('kmuttsit.net', 'kmuttsit.net'))
    )
    email_domain.widget.attrs['class'] = 'input-medium'

    password = forms.CharField(label='', widget=forms.PasswordInput)
    password_confirm = forms.CharField(label='', widget=forms.PasswordInput)

    helper = FormHelper()
    helper.layout = Layout(
        Field('full_name', placeholder='Full name', css_class='input-xlarge'),
        AppendedText('email', '@'),
        #'email_domain',
        Field('password', placeholder='Password', css_class='input-xlarge'),
        Field(
            'password_confirm',
            placeholder='Confirm password',
            css_class='input-xlarge'),
        Submit('submit', 'Sign up', css_class="btn btn-success pull-right")
    )
    """
