from django.contrib import auth
from django.contrib.auth.forms import AuthenticationForm
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext

from ans_gcm.models import ANSUser

#from datetime import datetime
#from forms import MessageForm
from accounts.forms import RegisterForm


def home(request):
    #now = datetime.now()
    #return HttpResponse(now.strftime("%c"))
    if request.user.is_authenticated():
        try:
            request_user = ANSUser.objects.get(username=request.user.username)
        except ANSUser.DoesNotExist:
            # admin checking
            if request.user.is_superuser:
                return HttpResponseRedirect("/admin")

        if request_user.profile_picture is not None:
            request.session['current_user'] = request_user
        if request.user.is_superuser:
            return HttpResponseRedirect("/admin")
        return render_to_response('main.html', {
            }, RequestContext(request))

    return render_to_response('index.html', {
        'form': AuthenticationForm()
    }, context_instance=RequestContext(request))


def test(request, template='test.html'):
    return render_to_response(template, {}, RequestContext(request))
