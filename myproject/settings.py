import os
# good pratice for access path via os.path
# in webfaction /home/attomos/webapps/ans/myproject/myproject
MYPROJECT = os.path.realpath(os.path.dirname(__file__))
# Django settings formyproject project.

DEBUG = True
TEMPLATE_DEBUG = DEBUG

EMAIL_HOST = 'smtp.webfaction.com'
EMAIL_HOST_USER = 'attomos'
EMAIL_HOST_PASSWORD = 'bookatom2144'
DEFAULT_FROM_EMAIL = 'no-reply@appnosystem.com'
SERVER_EMAIL = 'no-reply@appnosystem.com'

# API key - https://code.google.com/apis/console (Key for server apps)
# Key for browser apps (with referers)
GCM_APIKEY = "AIzaSyBzSo02XwIaWzkPcnIf-Vy_ni7msueV_bU"
# Key for server apps (with IP locking) not work, doc lie
#GCM_APIKEY = "AIzaSyCOmIM9lQnkwpFf5cYO8iymCaqDR0ZXF3s"

# RECAPTCHA
RECAPTCHA_PUBLIC_KEY = '6LdojNgSAAAAAIPO5dKagp9R6lF6UBS78zu-srzw'
RECAPTCHA_PRIVATE_KEY = '6LdojNgSAAAAAEqd7Cym3i3VgUe5NIHdIu4xQox8'
ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

# get rid off www subdomain session cookie
#SESSION_COOKIE_DOMAIN = 'appnosystem.com'
SESSION_COOKIE_DOMAIN = None


LOGIN_REDIRECT_URL = '/'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': 'unix:/home/attomos/memcached.soc'
    }
}

LOGIN_REQUIRED_URLS = (
    r'/agenda/$',
    r'/conversation/(?P<id>\d+)/$',
    r'/contacts/(.*)$',
    r'/messages/(.*)$',
    r'/status/$',
    r'/update_profile/$',
    r'/accounts/user_profile/$'
)

LOGIN_REQUIRED_URLS_EXCEPTIONS = (
    r'/accounts/register2',
    r'/accounts/activate',
    r'/accounts/login',
    r'/login',
    r'/logout'
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'ans.sqlite',                      # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Asia/Bangkok'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = False

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(MYPROJECT, 'media')
#MEDIA_ROOT = '/home/attomos/webapps/ans/myproject/myproject/media/'

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = ''

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    '/Users/attomos/git_repos/django-ans-static/',
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
    'dajaxice.finders.DajaxiceFinder',

)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'oih#bu7yrxxg-zljxr1!bu0)$#pkwft2v+rph&amp;@bc$f=@!^^69'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.request',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'myproject.middleware.RequireLoginMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'tracking.middleware.VisitorCleanUpMiddleware',
    'tracking.middleware.UserRestrictMiddleware',
    #'subdomains.middleware.SubdomainURLRoutingMiddleware',
)

ROOT_URLCONF = 'myproject.urls'
SUBDOMAIN_URLCONFS = {
    None: ROOT_URLCONF,
    'api': 'api.urls',
}

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'myproject.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    '/Users/attomos/Documents/DjangoProjects/ans/templates'
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    'api',
    'ans_gcm',
    'accounts',
    'south',
    'easy_thumbnails',
    'tracking',
    'crispy_forms',
    'classytags',
    'dajaxice',
    'dajax',
    #'captcha'
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
