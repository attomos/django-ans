from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from forms import DocumentForm

from ans_gcm.models import ApiKey
from models import Document
from piston.doc import generate_doc

from handlers import UserHandler, ApiKeyHandler, RegistrationHandler

def reset_api_key(request):
    return HttpResponse("Reset API key <a href='/'>Return home</a>")

def docs(request):
    doc = generate_doc(UserHandler)

    print doc.name # -> 'UserHandler'
    #print doc.model # -> <class 'ANSUser'>
    print doc.resource_uri_template # -> '/api/user/'
    return HttpResponse(doc.name)

@login_required
def list(request):
    # Handle file upload
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = Document(docfile = request.FILES['docfile'])
            newdoc.save()

            # Redirect to the document list after POST
            return HttpResponseRedirect(reverse('api.views.list'))
    else:
        form = DocumentForm() # An empty, unbound form

    # Load documents for the list page
    documents = Document.objects.all()

    # Render list page with the documents and the form
    return render_to_response(
            'list.html',
            {'documents': documents, 'form': form},
            context_instance=RequestContext(request))
