# -*- encoding: utf-8 -*-
from ans_gcm.gcm_sender import send_json_gcm_message
from ans_gcm.models import ANSUser, Agenda, ApiKey, Device, Message
from ans_gcm.models import Group, Membership

from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.conf import settings
from django.db.models import Q  # Max, Count, Q
from piston.utils import rc  # , require_mime, require_extended
from piston.handler import BaseHandler, AnonymousBaseHandler

import simplejson as json
from PIL import Image


class UserHandler(BaseHandler):
    """
    Entry point for API key authentication.
    Access via /api/user/
    """
    model = ANSUser
    anonymous = 'AnonymousUserHandler'
    fields = ('username', 'asterisk_username',
            'first_name', 'last_name', 'email',
            'location', 'status', 'availability',
            'user_type', 'profile_picture_url')

    @classmethod
    def profile_picture_url(self, ansuser):
        if ansuser.profile_picture:
            return "http://appnosystem.com/media/" + str(
                    ansuser.profile_picture)
        else:
            return ""

    @classmethod
    def availability(self, ansuser):
        # Return user availability as string
        return ANSUser.USER_STATUSES[ansuser.status][1]

    def read(self, request):
        """
        Returns all ANS users except user `asterisk`.
        """
        all_users = ANSUser.objects.filter(~Q(username='asterisk'),
                ~Q(username=request.user)).order_by('first_name', 'last_name')
        if 'filter_lecturer' in request.GET:
            return all_users.filter(user_type=0)
            return ANSUser.objects.filter(~Q(username='asterisk'),
                    ~Q(username=request.user), user_type=0).order_by(
                            'first_name', 'last_name')
        if 'filter_staff' in request.GET:
            return all_users.filter(user_type=1)
        if 'filter_student' in request.GET:
            return all_users.filter(user_type=2)
        return all_users
        return ANSUser.objects.filter(~Q(username='asterisk'),
                ~Q(username=request.user)).order_by('first_name', 'last_name')

    def create(self, request):
        """
        Returns the user information for request user.
        """
        if 'change_profile' in request.POST:
            # set status, availability, and place
            current_user = ANSUser.objects.get(username=request.user.username)
            current_user.status = request.POST.get('availability')
            current_user.location = request.POST.get('place')
            current_user.save()
        elif 'get_profile_picture' in request.POST:
            sender = request.POST.get('username')
            try:
                desired_user = ANSUser.objects.get(username=sender)
            except ANSUser.DoesNotExist:
                return HttpResponse('%s does not exist' % sender)
            return desired_user
        # for POST authentication
        return ANSUser.objects.get(username=request.user)


class AnonymousUserHandler(AnonymousBaseHandler):
    """
    Entry point for get request user information
    use basic authentication.
    access via /api/user/
    """
    model = ANSUser
    fields = ('username', 'asterisk_username',
            'first_name', 'last_name', 'email',
            'status', 'availability', 'user_type')

    @classmethod
    def availability(self, ansuser):
        # Return user availability as string
        return ANSUser.USER_STATUSES[ansuser.status][1]

    def read(self, request):
        """
        Returns the user information for request user.
        """
        try:
            current_user = ANSUser.objects.get(username=request.user)
        except:
            return HttpResponse("AnonymousUser")
        return current_user


class ApiKeyHandler(BaseHandler):
    """
    For create API key and get existing API key.
    access via /api/apikey/
    """
    model = ApiKey
    fields = ('key', )
    allowed_methods = ('GET', 'POST')

    def read(self, request):
        # Return the API key for request.user
        if request.user.keys.count() <= 0:
            return HttpResponse("Please use POST method instead.")

        values_query_set = request.user.keys.values('key')
        api_key = list(values_query_set)[0]['key']
        return HttpResponse(api_key)

    def create(self, request):
        '''
        Create a new API Key.
        Also prevent inactive user to create new API key.
        '''
        if request.user.is_active is False:
            return HttpResponse(
                    "{0} is not activated.\n Please check {1}.".format(
                    str(request.user.username), str(request.user.email)))

        # Check if API key already exists
        if request.user.keys.count() > 0:
            values_query_set = request.user.keys.values('key')
            api_key = list(values_query_set)[0]['key']
            return HttpResponse(api_key)
        else:
            # Create API key
            api_key = ApiKey(user=request.user)
            api_key.save()
        return HttpResponse(api_key)


class CalcHandler(BaseHandler):
    allowed_methods = ('POST')

    def create(self, request, expression):
        try:
            return HttpResponse(eval(expression))
        except:
            return HttpResponse("Wrong input! You noob!")


class RegistrationHandler(BaseHandler):
    """
    Handle the registration sent from Android devices.
    access via /api/register/
    """
    model = Device

    def read(self, request):
        return HttpResponse("User POST method instead")

    def create(self, request):
        if 'register_on_server' in request.POST:
            try:
                registration_id = request.POST['registration_id']
                phone_id = request.POST['phone_id']
                username = request.user
                account_name = request.POST['account_name']
            except KeyError:
                return HttpResponse("Bad request.", None, 400)

            try:
                device = Device.objects.get(identifier=phone_id)
                resp_text = "updated!"
            except Device.DoesNotExist:
                device = Device.objects.create(identifier=phone_id)
                resp_text = "created!"

            device.registration_id = registration_id
            device.user = ANSUser.objects.get(username=username)
            device.account_name = account_name
            device.save()  # save created or updated device
            return HttpResponse("Device[%s]'s registration ID is %s" %
                    (phone_id, resp_text))
        elif 'unregister_on_server' in request.POST:
            try:
                phone_id = request.POST['phone_id']
            except KeyError:
                return HttpResponse("Bad request", None, 400)

            try:
                device = Device.objects.get(identifier=phone_id)
                resp_text = "deleted!"
            except Device.DoesNotExist:
                return HttpResponse("The given device ID does not exist.",
                        None, 400)
            device.delete()  # delete device from database

            return HttpResponse("Device[%s]'s registration ID is %s" %
                    (phone_id, resp_text))


class MessageHandler2(BaseHandler):
    #FIXME or asterisk username
    """
    Entrypoint for ANS message handling.
    Send message API (recipient name  will be used rather than device ID)
    access via /api/send_message_to/recipient/
    """

    def read(self, request, recipient):
        return HttpResponse("read")

    def create(self, request, recipient):
        sender = request.user

        if request.POST['missed_call'] == 'true':
            try:
                # use asterisk_username instead of ANS username
                # this should be only lecturer and staff like 9881
                recipient_user = ANSUser.objects.get(
                        asterisk_username=recipient)
            except ANSUser.DoesNotExist:
                return HttpResponse(
                        "Extension '%s' is not registered with ANS, "
                        "visit http://appnosystem.com for more information." %
                        recipient)
        else:
            try:
                recipient_user = ANSUser.objects.get(username=recipient)
            except ANSUser.DoesNotExist:
                return HttpResponse("Unknown recipient", None, 400)

        if unicode(sender) == unicode(recipient_user):
            return HttpResponse("'%s' cannot send message to '%s'" %
                    (sender, recipient))
        sender_user = ANSUser.objects.get(username=sender)

        recipient_devices = Device.objects.filter(user=recipient_user)

        message_body = request.POST['message_body']
        # ANS can send message even user do not own any Android devices.
        if len(recipient_devices) == 0:
            message = Message.objects.create(sender=sender_user,
                    recipient=recipient_user,
                    message_status=0)
            message.recipient = recipient_user
            message.body = message_body
            message.save()
            # check whether message have agenda or not?
            if request.POST['agenda'] == 'true':
                if ('date_value' in request.POST and
                        'time_value' in request.POST):
                    date = request.POST['date_value'] + ' ' + request.POST[
                            'time_value']
                else:
                    date = request.POST['date']
                place = request.POST['place']
                agenda = Agenda.objects.create(message_id=message,
                        datetime=date)
                agenda.place = place
                agenda.save()
            if request.POST['missed_call'] == 'true':
                # caller identity from SIP client
                asterisk_user = request.POST['asterisk_user']
                try:
                    # check for asterisk_username first
                    sender_user = ANSUser.objects.get(
                            asterisk_username=asterisk_user)
                except ANSUser.DoesNotExist:
                    # then check for username
                    # student case
                    try:
                        sender_user = ANSUser.objects.get(
                                username=asterisk_user)
                    except ANSUser.DoesNotExist:
                        # worst case
                        # asterisk_username and username was not
                        # found in ANS database
                        # change sender user to asterisk server
                        sender_user = ANSUser.objects.get(username='asterisk')

                # message_status=1 indicates missed call message
                message = Message.objects.create(sender=sender_user,
                        recipient=recipient_user,
                        message_status=1)
                if 'missed_call_photo' in request.FILES:
                    #FIXME if not work
                    message.missed_call_photo = request.FILES[
                            'missed_call_photo']
                message.body = message_body
                message.save()
            return HttpResponse(
                    "Message is sent to %s (No Android device)"
                    % recipient_user)

        recipient_devices_list = []
        registration_ids_list = []
        for device in recipient_devices:
            recipient_devices_list.append(device)
            registration_ids_list.append(device.registration_id)

        # at this point no errors then create message
        if request.POST['missed_call'] == 'true':
            # caller identity from SIP client
            asterisk_user = request.POST['asterisk_user']
            try:
                # check for asterisk_username first
                sender_user = ANSUser.objects.get(
                        asterisk_username=asterisk_user)
            except ANSUser.DoesNotExist:
                # then check for username
                # student case
                try:
                    sender_user = ANSUser.objects.get(username=asterisk_user)
                except ANSUser.DoesNotExist:
                    # worst case
                    # no asterisk_username and username found in ANS database
                    # change sender user to asterisk server
                    sender_user = ANSUser.objects.get(username='asterisk')

            # message_status=1 indicates missed call message
            message = Message.objects.create(sender=sender_user,
                    recipient=recipient_user,
                    message_status=1)
            if 'missed_call_photo' in request.FILES:
                message.missed_call_photo = request.FILES['missed_call_photo']
            message.body = message_body
            json_data = json.dumps({"collapse_key": "message",
                "data": {
                    "message": message.body,
                }, "registration_ids": registration_ids_list
            })
        else:
            # normal message
            message = Message.objects.create(sender=sender_user,
                    recipient=recipient_user,
                    message_status=0)
            message.body = message_body
            json_data = json.dumps({"collapse_key": "message",
                "data": {
                    "message": message.body,
                }, "registration_ids": registration_ids_list
            })

        message.save()
        if request.POST['agenda'] == 'true':
            if 'date_value' in request.POST and 'time_value' in request.POST:
                date = request.POST['date_value'] + ' ' + request.POST[
                        'time_value']
            else:
                date = request.POST['date']
            place = request.POST['place']
            agenda = Agenda.objects.create(message_id=message,
                    datetime=date)
            agenda.place = place
            agenda.save()
            json_data = json.dumps({"collapse_key": "message",
                "data": {
                    "message": message.body,
                    "agenda": agenda.datetime,
                }, "registration_ids": registration_ids_list
            })
        return send_json_gcm_message(settings.GCM_APIKEY, json_data)
        return HttpResponse("%d message(s) were sent to %s" %
                (len(recipient_devices_list), recipient_user))


class MessageHandler3(BaseHandler):
    """
    Fetch messages API
    access via /api/messages/
    """
    model = Message
    fields = ('body', 'sender', 'recipient', 'created_on', 'message_status',
             'missed_call_photo_url', 'message_status_text', 'agenda_json',)

    @classmethod
    def missed_call_photo_url(self, message):
        if message.missed_call_photo:
            return "http://appnosystem.com/media/" + str(
                    message.missed_call_photo)
        else:
            return ""

    @classmethod
    def agenda_json(self, message):
        '''
        Return agenda JSON object
        if message contains agenda.
        We do this because some message have agenda and some don't.
        So, we need to check before response the messages data
        '''
        try:
            agenda_json_object = json.dumps(
                    {
                        'agenda_id': message.agenda.id,
                        'place': message.agenda.place,
                        'datetime': str(message.agenda.datetime),
                        'confirm': message.agenda.confirm
                    },)
                    #sort_keys=True,
                    #indent=4 * ' ')
            return agenda_json_object
        except Agenda.DoesNotExist:
            return 'None'

    @classmethod
    def message_status_text(self, message):
        # Return the message status string
        return Message.MESSAGE_STATUSES[message.message_status][1]

    def read(self, request):
        """
        Return latest messages between the request user and another user
        """
        request_user = ANSUser.objects.get(username=request.user)
        message_list = Message.objects.filter(
                Q(recipient=request_user) |
                Q(sender=request_user))
        unique_sender_set = set()
        latest_messages = []
        for message in message_list:
             # filter only sender name
            unique_sender_set.add(message.sender)
            unique_sender_set.add(message.recipient)
        unique_sender_list = list(unique_sender_set)
        # ignore the request user
        # work around for list.remove(x): x not in list
        try:
            unique_sender_list.remove(request_user)
        except ValueError:
            pass

        for i in range(len(unique_sender_list)):
            sender = ANSUser.objects.get(username=unique_sender_list[i])
            recipient = ANSUser.objects.get(username=request.user.username)
            latest_messages.extend(
                    Message.objects.filter(
                        Q(recipient=recipient, sender=sender) |
                        Q(recipient=sender,
                            sender=recipient)).order_by('-created_on')[:1])

        return sorted(latest_messages, key=lambda message: message.created_on,
                reverse=True)

    def create(self, request):
        """
        Returns messages for the request user.

        """
        request_user = ANSUser.objects.get(username=request.user)
        if request.POST['latest_messages'] == 'true':
            """
            Return latest messages between the request user and another user
            """

            #message_list = Message.objects.filter(recipient=request_user)
            message_list = Message.objects.filter(
                    Q(recipient=request_user) |
                    Q(sender=request_user))
            unique_sender_set = set()
            latest_messages = []
            # can do it by Opp
            #return Message.objects.filter(recipient=request_user)
            #.values('sender').annotate(Max('created_on'))
            for message in message_list:
                # filter only sender name
                unique_sender_set.add(message.sender)
                unique_sender_set.add(message.recipient)
            unique_sender_list = list(unique_sender_set)
            # ignore the request user
            unique_sender_list.remove(request_user)

            for i in range(len(unique_sender_list)):
                sender = ANSUser.objects.get(username=unique_sender_list[i])
                recipient = ANSUser.objects.get(username=request.user.username)
                latest_messages.extend(
                        Message.objects.filter(
                            Q(recipient=recipient, sender=sender) |
                            Q(recipient=sender,
                                sender=recipient)).order_by('-created_on')[:1])

        elif request.POST['latest_messages'] == 'false':
            """
            Return all messages between the request user and specified user
            """
            sender_username = request.POST['sender']
            try:
                sender_user = ANSUser.objects.get(username=sender_username)
            except ANSUser.DoesNotExist:
                return HttpResponse("Sender %s does not exist" %
                        (sender_username), None, 400)

            #latest_messages = Message.objects.filter(
            #        recipient=request_user, sender=sender_user)
            latest_messages = Message.objects.filter(
                    Q(recipient=request_user, sender=sender_user) |
                    Q(recipient=sender_user, sender=request_user))
            # TODO return conversation between request and sender_user
            # name this later
            return latest_messages
        else:
            return HttpResponse("Bad request")

        # sorted list of latest messages for all messages list
        return sorted(latest_messages, key=lambda message: message.created_on,
                reverse=True)


class AgendaHandler(BaseHandler):
    """
    Get confirmed agenda list
    /api/agenda/

    """
    model = Agenda
    fields = ('message_id', 'datetime', 'confirm', 'place')

    def read(self, request):
        ans_user = ANSUser.objects.get(username=request.user.username)
        all_message = Message.objects.all()
        agendas_list = []
        for message in all_message:
            if message.sender == ans_user or message.recipient == ans_user:
                agendas_list.extend(Agenda.objects.filter(message_id=message,
                    confirm=True))
        return agendas_list

    def create(self, request):
        ans_user = ANSUser.objects.get(username=request.user.username)
        if ('approve_agenda' in request.POST and
                request.POST['approve_agenda'] == 'true'):
            agenda_id = request.POST['agenda_id']
            that_agenda = Agenda.objects.get(id=agenda_id)
            that_agenda.confirm = True
            that_agenda.save()
            return HttpResponse('Agenda %s approved' % str(that_agenda))

        if ('delete_agenda' in request.POST and
                request.POST['delete_agenda'] == 'true'):
            agenda_id = request.POST['agenda_id']
            that_agenda = Agenda.objects.get(id=agenda_id)
            that_agenda.delete()
            return HttpResponse('Agenda %s deleted' % str(that_agenda))

        all_message = Message.objects.all()
        agendas_list = []
        if ('get_unconfirmed_agendas' in request.POST and
                request.POST['get_unconfirmed_agendas'] == 'true'):
            for message in all_message:
                if message.sender == ans_user or message.recipient == ans_user:
                    agendas_list.extend(Agenda.objects.filter(
                        message_id=message,
                        confirm=False))
                    return agendas_list
        for message in all_message:
            if message.sender == ans_user or message.recipient == ans_user:
                agendas_list.extend(Agenda.objects.filter(message_id=message,
                    confirm=True))
        return agendas_list


class ConversationHandler(BaseHandler):
    """
    Access via /api/conversation/id_or_name
    """
    model = Message
    fields = ('body', 'sender', 'recipient', 'created_on', 'message_status',
             'missed_call_photo_url', 'message_status_text', 'agenda_json',)

    @classmethod
    def missed_call_photo_url(self, message):
        if message.missed_call_photo:
            return "http://appnosystem.com/media/" + str(
                    message.missed_call_photo)
        else:
            return ""

    @classmethod
    def agenda_json(self, message):
        '''
        Return agenda JSON object
        if message contains agenda.
        We do this because some message have agenda and some don't.
        So, we need to check before response the messages data
        '''
        try:
            agenda_json_object = json.dumps(
                    {
                        'agenda_id': message.agenda.id,
                        'place': message.agenda.place,
                        'datetime': str(message.agenda.datetime),
                        'confirm': message.agenda.confirm
                    },)
                    #sort_keys=True,
                    #indent=4 * ' ')
            return agenda_json_object
        except Agenda.DoesNotExist:
            return 'None'

    @classmethod
    def message_status_text(self, message):
        # Return the message status string
        return Message.MESSAGE_STATUSES[message.message_status][1]

    def read(self, request, id_or_name):
        if type(id_or_name) is str:
            return HttpResponse('bookkung eiei')
        sender = ANSUser.objects.get(pk=id_or_name)
        recipient = ANSUser.objects.get(username=request.user.username)
        latest_messages = Message.objects.filter(
                Q(recipient=recipient, sender=sender) |
                Q(recipient=sender, sender=recipient)).order_by('created_on')
        return latest_messages

    def create(self, request, id_or_name):
        if type(id_or_name) is unicode:
            try:
                sender = ANSUser.objects.get(username=id_or_name)
            except ANSUser.DoesNotExist:
                return HttpResponse("Unknown recipient", None, 400)
        else:
            sender = ANSUser.objects.get(pk=id_or_name)
        recipient = ANSUser.objects.get(username=request.user.username)
        latest_messages = Message.objects.filter(
                Q(recipient=recipient, sender=sender) |
                Q(recipient=sender, sender=recipient)).order_by('created_on')
        return latest_messages


class GroupsHandler(BaseHandler):
    """
    API endpoint for groups management.
    Access via /api/groups/
    """
    model = Group

    def read(self, request, id):
        """
        Returns list of members in the group id=`id`
        """
        group = get_object_or_404(Group, pk=id)

        member_ships = []
        for member in Membership.objects.filter(group=group):
            member_ships.append(member)

        return member_ships

    def create(self, request):
        ans_user = ANSUser.objects.get(username=request.user.username)
        groups = Group.objects.filter(owner=ans_user)
        if request.POST['get_list_of_groups']:
            return groups

        if request.POST['add_member']:
            id = request.POST['group_id']
            group = get_object_or_404(Group, pk=id)
            print group
        return None


class MissedCallPhotoHandler(BaseHandler):
    """
    Missed call photo upload API
    Used by Asterisk server only
    /api/missedcall_photo/
    """
    allowed_methods = ('POST')

    def create(self, request):
        file_name = request.FILES['image'].name
        image = Image.open(request.FILES['image'])
        image.save(settings.MEDIA_ROOT + "/missedcall/" + file_name)
        return rc.ALL_OK
