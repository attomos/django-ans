# -*- encoding: utf-8 -*-
from django.conf.urls.defaults import patterns, url
from authentication import ApiKeyAuthentication
from handlers import CalcHandler
from handlers import UserHandler, ApiKeyHandler, RegistrationHandler
from handlers import MessageHandler2, MessageHandler3, ConversationHandler
from handlers import AgendaHandler, MissedCallPhotoHandler, GroupsHandler

from piston.resource import Resource
from piston.authentication import HttpBasicAuthentication

from views import reset_api_key, docs, list

key_auth = ApiKeyAuthentication()


def ProtectedResource(handler):
    return Resource(handler=handler, authentication=key_auth)

user = ProtectedResource(UserHandler)

auth = HttpBasicAuthentication(realm="Basic Authentication API")
apikey = Resource(handler=ApiKeyHandler, authentication=auth)

calc_resource = ProtectedResource(CalcHandler)

register_api = ProtectedResource(RegistrationHandler)
#send_message_resource = ProtectedResource(MessageHandler)
send_message_resource2 = ProtectedResource(MessageHandler2)
send_message_resource3 = ProtectedResource(MessageHandler3)
conversation_api = ProtectedResource(ConversationHandler)
ageda_api = ProtectedResource(AgendaHandler)
missedcall_photo_api = ProtectedResource(MissedCallPhotoHandler)
groups_api = ProtectedResource(GroupsHandler)

urlpatterns = patterns('',
    url(r'^apikey/$', apikey, name='api.apikey'),

    url(r'^user/$', user, name='api.user'),

    url(r'^calc/(?P<expression>.*)/$', calc_resource),

    url(r'^docs/$', docs),

    url(r'^reset_api_key/$', reset_api_key, name='api.reset_api_key'),

    url(r'^register/$', register_api),

    #url(r'^send_message/(?P<device_id>.*)/$', send_message_resource),

    url(r'^send_message_to/(?P<recipient>.*)/$', send_message_resource2),

    url(r'^messages/$', send_message_resource3),

    url(r'^conversation/(?P<id_or_name>.*)/$', conversation_api),

    url(r'^agenda/$', ageda_api),

    url(r'^missedcall_photo/$', missedcall_photo_api),

    url(r'^groups/$', groups_api),

    url(r'^upload_file/$', list, name='list'),

)
