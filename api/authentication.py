from django.http import HttpResponse
from django.contrib.auth.models import AnonymousUser
from django.shortcuts import _get_queryset
from ans_gcm.models import ApiKey


class ApiKeyAuthentication(object):

    def is_authenticated(self, request):
        auth_string = request.META.get("HTTP_AUTHORIZATION")

        if not auth_string:
            return False

        key = get_object_or_None(ApiKey, key=auth_string)

        if not key:
            request.user = AnonymousUser()
            return False

        request.user = key.user

        return True

    def challenge(self):
        resp = HttpResponse("Authorization Required")
        resp['WWW-Authenticate'] = "Key Based Authentication"
        resp.status_code = 401
        return resp


def get_object_or_None(klass, *args, **kwargs):
    queryset = _get_queryset(klass)
    try:
        return queryset.get(*args, **kwargs)
    except queryset.model.DoesNotExist:
        return None
