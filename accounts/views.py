# -*- encoding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.contrib import auth
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render_to_response
from django.template import Context, RequestContext
from django.views.decorators.csrf import csrf_exempt

from ans_gcm.models import ANSUser
from threading import Thread

from forms import RegisterForm, UserProfileForm
from activation import activate_user, send_activation


@csrf_exempt
def login(request):
    next = request.GET.get('next', '/')
    login_state = ''
    if request.user.is_authenticated():
        # redirect to root if user is already authenticated.
        return HttpResponseRedirect("/")
    if request.POST:
        username = request.POST.get('username', '')
        # email authentication
        if username.find('@') > 0:
            try:
                username = ANSUser.objects.get(email=username)
            except ANSUser.DoesNotExist:
                pass
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if user is not None and user.is_active:
            # Correct password and the user is active
            auth.login(request, user)
            login_state = 'good'
            # if not remember me, clear session when close browser
            request.session['current_user'] = ANSUser.objects.get(
                    username=username)
            # remember me
            if not request.POST.get('remember_me', None):
                request.session.set_expiry(0)
            # TODO handle next url
            '''
            if 'next' in request.GET:
                next = request.GET.get('next')
                return HttpResponseRedirect(next)
            if 'next' in request.POST:
                next = request.POST.get('next')
                return HttpResponseRedirect(next)
            '''
            return HttpResponseRedirect(next)
        else:
            login_state = 'bad'
    return render_to_response("registration/login.html",
            {'login_state': login_state,
                'username': request.POST.get('username', ''),
                },
            RequestContext(request))

'''
we won't use this
def register1(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            # success page and tell user to check e-mail
            return HttpResponseRedirect("/")
    else:
        form = RegisterForm()

    return render_to_response("registration/register.html",
            RequestContext(request, {'form': form, }))
'''


def register2(request):
    '''
    New ANS registration method
    kinda workaround
    '''
    if request.POST:
        fullname = request.POST.get('fullname')
        first_name = fullname.split(' ')[0]
        last_name = fullname.split(' ')[1]
        email = request.POST.get('email')  # used as username when login
        email_domain = request.POST.get('email_domain')
        password = request.POST.get('password')
        password_confirm = request.POST.get('password_confirm')

        # username check
        try:
            ANSUser.objects.get(username=email)
            return HttpResponse('user ' + email + ' already exist')
        except ANSUser.DoesNotExist:
            pass

        real_email = email + '@' + email_domain
        # email check
        try:
            ANSUser.objects.get(email=real_email)
            return HttpResponse(real_email + ' already exist')
        except ANSUser.DoesNotExist:
            pass

        if password != password_confirm:
            return HttpResponse("password and confirm password did not match")

        # check user type
        # lec 0
        # sta 1
        # stu 2
        user_type = 0
        if email_domain == 'st.sit.kmutt.ac.th':
            user_type = 2
        elif email_domain == 'sit.kmutt.ac.th':
            '''
            because lecturer do not need to do this
            admin will do it for them
            '''
            user_type = 1

        user = ANSUser()
        user.username = email
        user.first_name = first_name
        user.last_name = last_name
        user.email = real_email
        user.set_password(password)
        user.user_type = user_type
        user.is_active = False

        thread = Thread(target=send_activation, args=[user])
        thread.setDaemon(True)
        thread.start()
        user.save()

        return HttpResponse("200")
    return HttpResponse("POST please")


def activate(request):
    username = request.GET.get('username')
    code = request.GET.get('code')

    if activate_user(username, code):
        # welcome page
        template = 'registration/activation_complete.html'
        return render_to_response(template, {}, RequestContext(request))
    else:
        raise Http404


def forgot_username_or_password(request, template='accounts/forgot.html'):
    return render_to_response(template, {}, RequestContext(request))


def user_profile(request, template='accounts/user_profile.html'):
    user = ANSUser.objects.get(username=request.user.username)
    if request.POST:
        form = UserProfileForm(request.POST, request.FILES, instance=user)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('accounts.user_profile'))
    else:
        form = UserProfileForm(instance=user)
    return render_to_response(template,
            {'form': form}, RequestContext(request))
