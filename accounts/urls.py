# -*- encoding: utf-8 -*-
from django.conf.urls import patterns, url
from django.contrib.auth.views import login, logout

urlpatterns = patterns('accounts.views',
    url(r'^login/$', 'login', name='accounts.login'),
    url(r'^logout/$', logout, name='accounts.logout'),
    #url(r'^register/$', 'register1', name='accounts.register1'),
    url(r'^register2/$', 'register2', name='accounts.register2'),
    url(r'^activate/$', 'activate', name='activate'),

    url(r'^user_profile/$', 'user_profile', name='accounts.user_profile'),

)
