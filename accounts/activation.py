# -*- encoding: utf-8 -*-
from django.core.mail import send_mail
from django.contrib.auth.models import User
from hashlib import md5
from django.template import loader, Context
import time


def send_activation(user):
    time.sleep(5)
    code = md5(user.username).hexdigest()
    url = ("http://appnosystem.com/accounts/activate/?username=%s&code=%s" %
            (user.username, code))
    print url
    template = loader.get_template('registration/activation.html')
    context = Context({
        'username': user.username,
        'url': url,
    })

    send_mail('Activate new ANS account.', template.render(context),
            'no-reply@appnosystem.com', [user.email])


def activate_user(username, code):
    # use try to prevent the TypeError when user edit activation url
    try:
        if code == md5(username).hexdigest():
            user = User.objects.get(username=username)
            user.is_active = True
            user.save()
            return True
        else:
            return False
    except TypeError:
        return False
