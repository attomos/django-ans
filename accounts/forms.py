# -*- coding: utf-8 -*-
from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.contrib.auth.models import User
from ans_gcm.models import ANSUser
from activation import send_activation
from threading import Thread
from easy_thumbnails.fields import ThumbnailerImageField

from ans_gcm.widgets import AdminImageWidget

class RegisterForm(UserCreationForm):
    # 2012 Aug 14 try to replace User with ANSUser
    # TODO complete it
    #username = forms.CharField(label='')
    #username.widget.attrs['placeholder'] = 'Username'
    #email = forms.EmailField(label='')
    #email.widget.attrs['placeholder'] = 'E-mail'

    class Meta:
        model = ANSUser
        fields = ("username", "email", "user_type")

    def clean_email(self):
        email = self.cleaned_data["email"]
        if email.endswith('@st.sit.kmutt.ac.th'):
            pass
        elif email.endswith('@sit.kmutt.ac.th'):
            pass
        elif email.endswith('@vip.sit.kmutt.ac.th'):
            pass
        elif email.endswith('@kmuttsit.net'):
            pass
        else:
            raise forms.ValidationError('Invalid email domain')

        try:
            ANSUser.objects.get(email=email)
        except ANSUser.DoesNotExist:
            return email

        raise forms.ValidationError("A user with that email address already exists.")

    def save(self):
        user = super(RegisterForm, self).save(commit=False)
        user.is_active = False

        thread = Thread(target=send_activation, args=[user])
        thread.setDaemon(True)
        thread.start()
        user.save()


class UserProfileForm(forms.ModelForm):

    class Meta:
        model = ANSUser
        #fields = ('first_name', 'last_name', 'email', 'profile_picture',
        #        'asterisk_username')

    def __init__(self, *args, **kw):
        super(UserProfileForm, self).__init__(*args, **kw)
        self.fields['profile_picture'].widget = AdminImageWidget()
        self.fields.keyOrder = [
            'first_name',
            'last_name',
            'email',
            'profile_picture',
            'asterisk_username'
        ]
        if self.instance.user_type == ANSUser.USER_TYPES[2][0]:
            self.fields.keyOrder.remove('asterisk_username')

    def save(self, *args, **kw):
        user = super(UserProfileForm, self).save(*args, **kw)
        user.save()
